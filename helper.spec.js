// reads the .env file and stores it as environment variables, use for config
require('dotenv').config()

const mongoose = require('mongoose')
const connect = require('./connect')

const User = require('./src/models/user.model')() // note we need to call the model caching function
const Equipment = require('./src/models/equipment.model')() // note we need to call the model caching function

// connect to the database
connect.mongoConnect(process.env.TEST_DB)
// connect.neoConnect(process.env.NEO4J_URL)

// drop both collections before each test
beforeEach(async () => {

    await Promise.all([User.deleteMany(), Equipment.deleteMany()])
});