const Equipment = require('../models/equipment.model')() // note we need to call the model caching function
const User = require('../models/user.model')() // note we need to call the model caching function
// const neo = require('../neo')


const services = {};


createEquipment = async ({ ...equipmentProperties }) => {
    // console.log(user);
    // equipmentProperties.postedBy = user._id;


    const newEquip = await Equipment.create(equipmentProperties);
  //  await neo.createEquipment(newEquip);


    return newEquip;
}

getAllEquipment = async () => {
    return await Equipment.find().populate('postedBy','username');
}

getEquipmentById = async(equipmentId) => {
    return await Equipment.findById(equipmentId)
}

updateEquipment = async (equipmentId, {...equipmentProperties}, user) => {
    const where = { _id: equipmentId };


    return await Equipment.findOneAndUpdate(where, equipmentProperties, {upsert: false, runValidators: true}, (err, result) => {
        if (result) return result;
        else return err;
    })
}

deleteEquipment = async (equipmentId, user) => {
    console.log(equipmentId)
    const where =  {_id: equipmentId}

    return await Equipment.findOneAndDelete(where, (err, result) => {
        if (result) {
          //  neo.deleteEquipment(result);
            return result;
        }
        else return err;

    })
}

// services.getlikes = async (user) => {
//     try {
//       return await neo.getLikes(user);
//     } catch {
//       return Promise.reject(new Error("Cannot get likes"));
//     }
//   }


// services.likeEquipment = async (equipmentId, user) => {
//     try {
//         return await neo.likeEquipment(equipmentId, user)
//     } catch {
//         return Promise.reject(new Error("Cannot like match"));
//       }
// }

// services.dislikeEquipment = async (equipmentId, user) => {
//     try {
//         return await neo.dislikeEquipment(equipmentId, user)
//     } catch {
//         return Promise.reject(new Error("Cannot dislike match"));
//       }
// }

// vanaf hieronder uncommenten straks 
// likesEquipment = async (equipId, user) => {
//   console.log(user);
//   return await neo
//   .session()
//   .run(`MERGE (u:User {id: "${user._id}"}) 
//   MERGE (d:Equipment {id: "${equipId}"}) 
//   MERGE (u)-[:LIKES]->(d)`)
// }


// dislikeEquipment = async (equipId, user) => {
  
//   return await neo
//   .session()
//   .run(`MERGE (u:User {id: "${user._id}"})-[r:LIKES]->(m:Equipment {id: "${equipId}"}) 
//   DELETE r`)
// }




// makeFriend = async (userId, user) => {
//     try {
//       return await neo.makeFriend(userId, user);
//     } catch {
//       return Promise.reject(new Error("Cannot make friend"));
//     }
//   }
//   deleteFriend  = async (userId, user) => {
//     try {
//       return await neo.deleteFriend(userId, user);
//     } catch {
//       return Promise.reject(new Error("Cannot make friend"));
//     }
//   }

//   getAllEquipmentLikedFromFriend  = async (userId, user) => {
//     try {
//       return await neo.getAllEqLikedFromFriend(userId, user);
//     } catch {
//       return Promise.reject(new Error("Cannot get friend"));
//     }
//   }





module.exports = {
  createEquipment,
  getAllEquipment,
  getEquipmentById,
  updateEquipment,
  deleteEquipment,
  // likesEquipment,
  // dislikeEquipment,
  // makeFriend,
  // deleteFriend,
  // getAllEquipmentLikedFromFriend
};
