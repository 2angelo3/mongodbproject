const Equipment = require('../models/equipment.model')()
const User = require('../models/user.model')() // note we need to call the model caching function
const wishlistModel = require('../models/wishlist.model')();
const neo = require('../neo');

let services = {};

services.getAll = async (req, res, next) => {
    const wishlists = await wishlistModel.find().populate('equipment', 'name').populate('user', 'username')

    res.status(200).send(wishlists);
}

services.getOne = async (req, res, next) => {
    const wishlist = await wishlistModel.findById(req.params.id).populate('equipment', ['name', 'description'])
    
    res.status(200).send(wishlist)
}

module.exports = services;