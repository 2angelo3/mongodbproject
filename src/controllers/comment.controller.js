// const Comment = require('../models/comment.schema')() // note we need to call the model caching function
const Equipment = require('../models/equipment.model')()
const User = require('../models/user.model')() // note we need to call the model caching function
const neo = require('../neo')


const services = {};


services.getAllComments = async (req, res, next) => {
    const populatedEquipemtn = await Equipment.findById(req.params.equipid).populate('comments.user', 'username');

    res.status(200).send(populatedEquipemtn.comments)
}

services.createComment = async (req, res, next) => {
    console.log(req.params.equipid)

    const equipment = await Equipment.findById(req.params.equipid, (err, docs) => {
        // Store old amount comments
        const commentLength = docs.comments.length;

        // Store the changes to the Equipment
        docs.save(docs.comments.push(req.body));
        // await neo.createComment(req.body);

        // Check if new comment was successfully addded
        if (docs.comments.length > commentLength) return docs;
        else return res.status(400).send("Comment wasn't added")
    })

    res.status(201).send(equipment);
}

services.getComment = async (req, res, next) => {
    return await Equipment.findById(req.params.equipid, (err, docs) => {
        const comment = docs.comments.find(com => com._id.toString() === req.params.id);

        return res.status(200).send(comment);
    }).populate('comments.user', 'username')
}

services.updateComment = async (req, res, next) => {
    console.log(req.params)

    const equipment = await Equipment.findById(req.params.equipid, (err, docs) => {
        const comment = docs.comments.find(com => com._id.toString() === req.params.id);

        comment.title = req.body.title;
        comment.text = req.body.text;

        docs.save(comment);

        return docs;
    });

    return res.status(200).send(equipment);
}

services.deleteComment = async (req, res, next) => {
    console.log(req.params)

    const equipment = await Equipment.findById(req.params.equipid, (err, docs) => {
        let comment = docs.comments.find(com => com._id.toString() === req.params.id);

        console.log(comment)

        docs.comments.remove(comment);
        docs.save();

        // await neo.deleteComment(comment)

        console.log(docs);

        return docs;
    });

    return res.status(200).send(equipment);
}


module.exports = services;
