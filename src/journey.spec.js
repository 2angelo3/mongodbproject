// const chai = require('chai')
// const expect = chai.expect

// const requester = require('../requester.spec')


// describe('user journeys', function() {
//     it('create Equipment; create user; user buys equipment; user leaves review', async function() {
//         let res

//         const testEquipment = {
//             name: 'Camera X120',
//             description: 'A cool camera',
//             price: 259
//         }
//         res = await requester.post('/equipment').send(testEquipment)
//         expect(res).to.have.status(201)
//         testEquipment.id = res.body.id

//         const testUser = {name: 'Joe'}

//         res = await requester.post('/user').send(testUser)
//         expect(res).to.have.status(201)
//         testUser.id = res.body.id

//         res = await requester.post(`/equipment/${testEquipment.id}/purchase`).send({user: testUser.name})
//         expect(res).to.have.status(201)

//         const testReview = {
//             user: testUser.name,
//             rating: 3,
//             text: 'Pretty average camera'
//         }
//         res = await requester.post(`/equipment/${testEquipment.id}/review`).send(testReview)
//         expect(res).to.have.status(201)

//         res = await requester.get(`/equipment/${testEquipment.id}`)
//         expect(res).to.have.status(200)
//         expect(res.body).to.have.property('name', testEquipment.name)
//         expect(res.body).to.have.property('description', testEquipment.description)
//         expect(res.body).to.have.property('price', testEquipment.price)
//         expect(res.body).to.have.property('reviews').and.to.have.length(1)
//         const equipmentReview = res.body.reviews[0]
//         expect(equipmentReview).to.have.property('rating', testReview.rating)
//         expect(equipmentReview).to.have.property('text', testReview.text)
//         expect(equipmentReview.user.toString()).to.equal(testUser.id)

//         res = await requester.get(`/user/${testUser.id}`)
//         expect(res).to.have.status(200)
//         expect(res.body).to.have.property('name', testUser.name)
//         expect(res.body).to.have.property('bought').and.to.be.length(1)
//         const userReview = res.body.bought[0]
//         expect(userReview).to.have.property('name', testEquipment.name)
//         expect(userReview).to.have.property('description', testEquipment.description)
//         expect(userReview).to.have.property('price', testEquipment.price)
//     })
// })