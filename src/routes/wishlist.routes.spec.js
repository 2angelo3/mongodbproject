const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

const User = require('../models/user.model')() // note we need to call the model caching function
const Wishlist = require('../models/wishlist.model')()
const Equipment = require('../models/equipment.model')() // note we need to call the model caching function

describe('wishlist endpoints', function() {
    describe('integration tests', function() {
        let testUser
        let testEquipment
    
        beforeEach(async function() {
            testUser = await new User({
                username: 'Angelo',
                email: 'Avp@server.nl',
                password: 'wachtwoord123'
            }).save()
    
            testEquipment = await new Equipment({
                name: 'dumbell 2',
                emailaddress: 'dumbel@server.nl',
                description: 'de dumbel van 25 kg ',
                comments:[],
            }).save()
        })


        it('(DELETE /wishlist/:id) should delete a wishlist', async function() {
            const testWishlist = new Wishlist({
                name: 'dumbell 2',
                description: 'wishlist for the sportequipments',
                equipment: testEquipment._id,
                user: testUser._id
            })
    
            await testWishlist.save()
    
            const res = await requester.delete(`/wishlist/${testWishlist.id}`)
    
            expect(res).to.have.status(200)
    
           
        })
        
    
        it('Cannot create a wishlist without a name.', async function() {
            const testwishlist = {
                
                description: 'wishlist for the sportequipments',
                equipment: testEquipment._id,
                user: testUser._id
            }
            const res = await requester.post(`/wishlist`).send(testwishlist)
            expect(res).to.have.status(400)
        })




    



        it('(POST /wishlist) should not create a equipment with missing name', async function() {
            const testwishlist = {
               
                description: 'wishlist for the sportequipments',
                equipment: testEquipment._id,
                user: testUser._id
            }
    
            const res = await requester.post('/wishlist').send(testwishlist)
    
            expect(res).to.have.status(400)
    
          
        })

        
    })


    

    describe('system tests', function() {
    })
})