const express = require('express')
const router = express.Router()

const Comment = require('../models/comment.schema')

// const CrudController = require('../controllers/crud')
// const CommentCrudController = new CrudController(Comment);

const commentController = require('../controllers/comment.controller')


// create a Comment
router.post('/:equipid', commentController.createComment);

// get all Comments
router.get('/:equipid/comments', commentController.getAllComments)

// // get a Comment
router.get('/:equipid/:id', commentController.getComment);

// // update a Comment
router.put('/:equipid/:id',  commentController.updateComment);

// // remove a Comment
router.delete('/:equipid/:id',  commentController.deleteComment);

module.exports = router
