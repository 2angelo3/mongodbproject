const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')
const Comment = require('../models/comment.schema')

const User = require('../models/user.model')() // note we need to call the model caching function
const Equipment = require('../models/equipment.model')() // note we need to call the model caching function

describe('Comment endpoints', function() {
    describe('integration tests', function() {
        let testUser
        let testEquipment
    
        beforeEach(async function() {
            testUser = await new User({
                username: 'Angelo van Peer',
                email: 'avp@server.nl',
                password: 'wachtwoord123'
            }).save()
    
            testEquipment = await new Equipment({
                name: 'Dumbel',
                emailAddress: 'dumbel@server.nl',
                description: 'dumbel is 25 kg',
                comments:[],
                
            }).save()
        })
    
        it('(POST /comment) should create a new comment', async function() {
            const testComment = {
                title: 'SportEquipment',
                text: 'Deze dumbel is heel zwaar',
                
               
            }

            const res = await requester.post(`/equipment/${testEquipment._id}`).send(testComment)
    
            expect(res).to.have.status(201)
    
            const equipment = await Equipment.findOne({_id:testEquipment._id})
            expect(equipment).to.have.property('name', testEquipment.name)
            expect(equipment).to.have.property('emailAddress', testEquipment.emailAddress)
            expect(equipment).to.have.property('description', testEquipment.description)
            expect(equipment).to.have.property('comments').of.length(1)
            const comment = equipment.comments[0]
            expect(comment).to.have.property('title', testComment.title)
            expect(comment).to.have.property('text', testComment.text)
            expect(comment).to.have.property('user')
           
        })



        it('(UPDATE /comment) should update comment', async function() {



            const firstComment = {
                _id: "6070d480c19c25460896d962",
                title: 'dumbell 2',
                text: 'dumbel@server.nl',
               
                
            }

            const secondComment = {
             
                title: 'Title van dumbel',
                text: 'dumbel@server.nl',
              
            }




            const res = await requester.put(`/equipment/${firstComment._id}`).send(secondComment)

            expect(res).to.have.status(200)

        })


        it('(DELETE /equipment/:commentid) should delete a Comment', async function() {
            const testComment = {
                title: 'Title van dumbel',
                text: 'dumbel@server.nl',
              
             
            }
    
            await testEquipment.save()
    
            const res = await requester.delete(`/equipment/${testEquipment.id}/comments/${testComment.id}`)
    
            expect(res).to.have.status(404)
    
          
        })


     

    
    
    
        
    })

    describe('system tests', function() {
    })
})