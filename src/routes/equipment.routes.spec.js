const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

const Equipment = require('../models/equipment.model')() // note we need to call the model caching function

describe('Equipment endpoints', function() {
    describe('integration tests', function() {
        it('(POST /equipment) should create a equipment', async function() {
            const testEquipment = {
             name: "test van equipment",
             description: "Beschrijving wishlist veranderen",
             emailAddress: "test"
              
            

                
            }

            const res = await requester.post('/equipment').send(testEquipment)

            expect(res).to.have.status(201)
    
            const equipment = await Equipment.findOne({name: testEquipment.name})
            expect(equipment).to.have.property('name', testEquipment.name)
            expect(equipment).to.have.property('description', testEquipment.description)
            expect(equipment).to.have.property('emailAddress', testEquipment.emailAddress)
            expect(equipment).to.have.property('comments').and.to.be.empty
        })

        it('(POST /equipment) should create a equipment with a promise chain', function() {
            const testEquipment = {
                name: 'Dumbel',
                emailAddress: 'dumbel@server.nl',
                description: 'dumbel is 25 kg',


                
            }

            return requester
                .post('/equipment')
                .send(testEquipment)
                .then(res => {
                    expect(res).to.have.status(201)
                    return Equipment.findOne({name: testEquipment.name})
                })
                .then(equipment => {
                    expect(equipment).to.have.property('name', testEquipment.name)
                    expect(equipment).to.have.property('emailAddress', testEquipment.emailAddress)
                    expect(equipment).to.have.property('description', testEquipment.description)
                    expect(equipment).to.have.property('comments').and.to.be.empty
                })
        })


        it('(DELETE /user/:id) should delete a Equipment', async function() {
            const testEquipment = new Equipment({
                name: 'Dumbeldelete',
                emailAddress: 'dumbeldelete@server.nl',
                description: 'dumbel is 25 kg',
             
            })
    
            await testEquipment.save()
    
            const res = await requester.delete(`/equipment/${testEquipment.id}`)
    
            expect(res).to.have.status(200)
    
            const equipment = await Equipment.findOne({name: testEquipment.name})
            expect(equipment).to.be.null
        })


        it('(UPDATE /Equipment) should update equipment', async function() {



            const firstEquipment = new Equipment ({
                _id: "6070d480c19c25460896d962",
                name: 'dumbell 2',
                emailAddress: 'dumbel@server.nl',
                description: 'de dumbel van 25 kg ',
                
            })

            const secondEquipment = new Equipment ({
             
                name: '333',
                emailAddress: 'dumbel@server.nl',
                description: 'de dumbel van 25 kg ',
            })




            const res = await requester.put(`/equipment/${firstEquipment._id}`).send(secondEquipment)

            expect(res).to.have.status(200)

        })
    
        it('(POST /equipment) should not create a equipment with missing name', async function() {
            const testEquipment = {
               
                emailAddress: 'dumbel@server.nl',
                description: 'dumbel is 25 kg',
                
            }
    
            const res = await requester.post('/equipment').send(testEquipment)
    
            expect(res).to.have.status(400)
    
            const count = await Equipment.find().countDocuments()
            expect(count).to.equal(0)
        })
    })

    describe('system tests', function() {
        it('should create and retrieve a equipment', async function() {
            const testEquipment = {
                name: 'Dumbel',
                emailAddress: 'dumbel@server.nl',
                description: 'dumbel is 25 kg',
               


                
            }

            const res1 = await requester.post('/equipment').send(testEquipment)
            expect(res1).to.have.status(201)
            const id = res1.body._id
            const res2 = await requester.get(`/equipment/${id}`)
            expect(res2).to.have.status(200)
            expect(res2.body).to.have.property('_id', id)
            expect(res2.body).to.have.property('name', testEquipment.name)
            expect(res2.body).to.have.property('emailAddress', testEquipment.emailAddress)
            expect(res2.body).to.have.property('description', testEquipment.description)
            expect(res2.body).to.have.property('comments').to.be.empty
        })
    })
})