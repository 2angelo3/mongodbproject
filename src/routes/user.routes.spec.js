const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

const User = require('../models/user.model')() // note we need to call the model caching function


describe('user endpoints', function() {
    describe('integration tests', function() {
        it('(POST /user) should create a new user', async function() {
            const testUser = {
                username: "Angelo",
                email: "avp@server.nl",
                password: "wachtwoord123",
                description: "22 jaar noord brabant"
            }
    
            const res = await requester.post('/user').send(testUser)
            expect(res).to.have.status(201)
            expect(res.body).to.have.property('id')
    
            const user = await User.findById(res.body.id)
            expect(user).to.have.property('username', testUser.username)
            expect(user).to.have.property('email', testUser.email)
            expect(user).to.have.property('password', testUser.password)
            expect(user).to.have.property('description', testUser.description)


        })
    
        it('(POST /user) should not create a user without a name', async function() {
            const testUser = {
        
                email: "avp@server.nl",
                password: "wachtwoord123",
                description: "22 jaar noord brabant"
            }
            const res = await requester.post('/user').send(testUser)
    
            expect(res).to.have.status(400)
    
            const docCount = await User.find().countDocuments()
            expect(docCount).to.equal(0)
        })

        it('(DELETE /user/:id) should delete a user', async function() {
            const testUser = new User({
                username: "Angelo delete",
                email: "avpdelete@server.nl",
                password: "wachtwoord123",
                description: "22 jaar noord brabant delete"
            })
    
            await testUser.save()
    
            const res = await requester.delete(`/user/${testUser.id}`)
    
            expect(res).to.have.status(200)
    
            const user = await User.findOne({username: testUser.name})
            expect(user).to.be.null
        })
   

        
    
      
    
        it('(GET /user/:id) should give a user', async function() {
            const testUser = new User({
                username: "Angelo",
                email: "avp@server.nl",
                password: "wachtwoord123",
                description: "22 jaar noord brabant"
            })
    
            await testUser.save()
    
            const res = await requester.get(`/user/${testUser._id}`)
    
            expect(res).to.have.status(200)
            expect(res.body).to.have.property('username', testUser.username)
            
        })


        it('should make a friend', async function() {
            const testUser = new User({
               
                username: 'Angelo',
                email: 'avp@gmail.com',
                description: 'avp@gmail.com',
                password: 'wachtwoord123'
            })

            const us = {
                user: {
                    _id: "6070c4f9dcfe583c0cbc8002"
                }
            }

            await testUser.save()

            const res = await requester.post(`/user/${testUser._id}/makefriend`).send(us)

            expect(res).to.have.status(200)
            expect(res.body).to.have.property('summary')
            expect(res.body).to.have.property('records')

        })
    
   
    })

})