const express = require('express')
const router = express.Router()

const Equipment = require('../models/equipment.model')() // note we need to call the model caching function

// const CrudController = require('../controllers/crud')

//const equipmentCrudController = new CrudController(Equipment)
const equipmentController = require('../controllers/equipment.controller')




// create a Equipment
router.post('/', (req, res, next) => {
    console.log(req.body);
    equipmentController.createEquipment(req.body)
    .then(equipment => res.status(201).send(equipment))
    .catch(error => res.status(400).send(error.message))
});

// get all Equipments
router.get('/', (req, res) => {
    equipmentController.getAllEquipment()
        .then((equipment) => res.status(200).send(equipment))
        .catch(error => res.status(400).send(error.message));
});

// get an Equipment by its id
router.get('/:id', (req, res, next) => {
    equipmentController.getEquipmentById(req.params.id)
        .then((equipments) => res.status(200).send(equipments))
        .catch(error => res.status(400).send(error.message));
});

// update a Equipment
router.put('/:id', (req, res, next) => {
    equipmentController.updateEquipment(req.params.id, req.body)
        .then((equipment) => res.status(200).send(equipment))
        .catch(error => res.status(400).send(error.message));
});

// remove a Equipment
router.delete('/:id', (req, res, next) => {
    equipmentController.deleteEquipment(req.params.id)
        .then((equipment) => res.status(200).send(equipment))
        .catch(error => res.status(400).send(error.message))
});


// router.post('/:equipmentId/like', (req, res) => {
//     likeService
//     .likeEquipment(req.params.equipmentId, req.body.user)
//     .then((result) => res.status(200).send(result))
//     .catch((error) => res.status(400).send(error.message ? error.message : error));
// });

// router.post('/:equipmentId/dislike', (req, res) => {
//     likeService
//     .dislikeEquipment(req.params.equipmentId, req.body.user)
//     .then((result) => res.status(200).send(result))
//     .catch((error) => res.status(400).send(error.message ? error.message : error));
// });


// router.post('/:userId/makefriend', (req, res) => {
//     equipmentController
//     .makeFriend(req.params.userId, req.body.user)
//     .then((result) => res.status(200).send(result))
//     .catch((error) => res.status(400).send(error.message ? error.message : error));
//   });
  
  
//   router.post('/:userId/deletefriend', (req, res) => {
//     equipmentController
//     .deleteFriend(req.params.userId, req.body.user)
//     .then((result) => res.status(200).send(result))
//     .catch((error) => res.status(400).send(error.message ? error.message : error));
//   });
//   router.get('/:userId/getalllikeddogsfromfriend', (req, res) => {
//     equipmentController
//     .getAllDogsLikedFromFriend(req.params.userId, req.body.user)
//     .then((result) => res.status(200).send(result))
//     .catch((error) => res.status(400).send(error.message ? error.message : error));
//   });

module.exports = router
