const express = require('express')
const router = express.Router()
const { check, validationResult} = require("express-validator/check");
const User = require('../models/user.model')() // note we need to call the model caching function
const bcrypt = require("bcryptjs");
const CrudController = require('../controllers/crud')
const jwt = require("jsonwebtoken");
const UserCrudController = new CrudController(User)
var neo = require('../neo');
const equipmentController = require('../controllers/equipment.controller')

// create a user
router.post('/', UserCrudController.create)

router.post(
    "/signup",
    [
        check("username", "Please Enter a Valid Username")
        .not()
        .isEmpty(),
        check("email", "Please enter a valid email").isEmail(),
        check("password", "Please enter a valid password").isLength({
            min: 6
        }),
        check("description", "Please enter a valid desc").isLength({
          min: 6
      })
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array()
            });
        }

        const {
            username,
            email,
            password,
            description
            
        } = req.body;
        try {
            let user = await User.findOne({
                email
            });
            if (user) {
                return res.status(400).json({
                    msg: "User Already Exists"
                });
            }

            user = new User({
                username,
                email,
                password,
                description
                
            });

            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(password, salt);

            await user.save();

            const payload = {
                user: {
                    id: user.id,
                    email: user.email
                }
            };

            // neo.createUser(user);

            jwt.sign(
                payload,
                process.env.JWT_SECRET_TOKEN, {
                    expiresIn: 10000
                },
                (err, token) => {
                    if (err) throw err;
                    res.status(200).json({
                        token,
                        _id: user._id,
                        username: user.username,
                        email: user.email,
                       
                        
                    });
                }
            );
        } catch (err) {
            console.log(err.message);
            res.status(500).send("Error in Saving");
        }
    }
);
router.post(
    "/login",
    [
      check("email", "Please enter a valid email").isEmail(),
      check("password", "Please enter a valid password").isLength({
        min: 6
      })
    ],
    async (req, res) => {
      const errors = validationResult(req);
  
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array()
        });
      }
  
      const { email, password } = req.body;
      try {
        let user = await User.findOne({
          email
        });
        if (!user)
          return res.status(400).json({
            message: "User Not Exist"
          });
  
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch)
          return res.status(400).json({
            message: "Incorrect Password !"
          });
  
        const payload = {
          user: {
            id: user.id
          }
        };

        jwt.sign(
          payload,
          process.env.JWT_SECRET_TOKEN,
          {
            expiresIn: 3600
          },
          (err, token) => {
            if (err) throw err;
            res.status(200).json({
              token,
              _id: user._id,
              username: user.username,
              email: user.email
            
      
            });
          }
        );
      } catch (e) {
        console.error(e);
        res.status(500).json({
          message: "Server Error"
        });
      }
    }
  );

  router.post('/:userId/makefriend', (req, res) => {
      equipmentController
    .makeFriend(req.params.equipmentId, req.body.user)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(400).send(error.message ? error.message : error));
});
router.post('/:userId/deletefriend', (req, res) => {
    equipmentController
    .deleteFriend(req.params.equipmentId, req.body.user)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(400).send(error.message ? error.message : error));
});

router.get('/:userId/getalllikedEquipmentsfromfriend', (req, res) => {
  equipmentController
  .getAllEquipmentsLikedFromFriend(req.params.userId, req.body.user)
  .then((result) => res.status(200).send(result))
  .catch((error) => res.status(400).send(error.message ? error.message : error));
});


// create a user
router.post('/', UserCrudController.create)

// get all users
router.get('/', UserCrudController.getAll)

// get a user
router.get('/:id', UserCrudController.getOne)

// update a user
router.put('/:id', UserCrudController.update)

// remove a user
router.delete('/:id', UserCrudController.delete);

module.exports = router