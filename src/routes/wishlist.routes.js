const express = require('express')
const router = express.Router()
const neo = require('../neo')

const Wishlist = require('../models/wishlist.model')() // note we need to call the model caching function
const wishListController = require('../controllers/wishlist.controller')

const CrudController = require('../controllers/crud')
const wishlistCrudController = new CrudController(Wishlist);


// create a WishList
router.post('/',  wishlistCrudController.create)

// get all WishList
router.get('/',  wishListController.getAll)

// get a WishList
router.get('/:id',  wishListController.getOne)

// update a WishList
router.put('/:id', wishlistCrudController.update)

// remove a WishList
router.delete('/:id', wishlistCrudController.delete)


module.exports = router
