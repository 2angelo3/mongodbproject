const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  // Get bearer from header
  const bearer = req.headers.authorization;

  // Split the bearer
  const splittedBearer = bearer.split('.');

  // Store the token inside a variable
  const token = splittedBearer[1];

  // Verify if the headers token was valid
  jwt.verify(token, process.env.JWT_SECRET_TOKEN, (error, user) => {

    if (error) return res.status(401).send('User not authenticated!');
    req.user = user;
    next();
  });
};
