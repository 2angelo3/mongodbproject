const neo = require('../connect');

const queries = {};


createUser = async (user) => {
    try {
        await neo
        .session()
        .run(`MERGE (u:User {id: "${user._id}"})`)
    } catch {
        return Promise.reject('Failed to create user')
    }
}

createEquipment = async (equipment) => {
    
    try {
        await neo
        .session()
        .run(`MERGE (e:Equipment {id: "${equipment._id}"})`)
    } catch {
        return Promise.reject('Failed to create Equipment')
    }
}


deleteEquipment = async (equipment) => {
    return await neo
    .session()
    .run(`MATCH (e:Equipment{id: ${equipment._id}}) DETACH DELETE e`)
}


likesEquipment = async (equipmentId, user) => {
    
    
        return await neo
        .session()
        .run(`MERGE (u:User {id: ${user._id}}) 
            MERGE (e:Equipment {id: ${equipmentId}}) 
            MERGE (u)-[:LIKES]->(e)`)

}


dislikeEquipment = async (equipmentId, user) => {
    
    return await neo
    .session()
    .run(`MERGE (u:User {id: "${user._id}"})-[r:LIKES]->(m:Equipment {id: "${equipmentId}"}) 
    DELETE r`)
}

makeFriend = async (userId, user) => {
    console.log(user);
    return await neo
    .session()
    .run(`MERGE (u:User {id: "${user._id}"}) 
    MERGE (ou:User {id: "${userId}"}) 
    MERGE (u)-[:FRIENDS]->(ou)`)
}

deleteFriend = async (userid, user) => {

    return await neo
    .session()
    .run(`MATCH (u:User {id: "${user._id}"})-[r:FRIENDS]->(ou:User {id: "${userid}"}) 
    DELETE r`)
}

getAllEquipmentLikedFromFriend = async (userId, user) => {
    console.log(user);
    return await neo
    .session()
    .run(`MATCH (u:User {id: "${user._id}"})-[:FRIENDS]->(ou:User)
    MATCH (ou:User{id: "${userId}"})-[:LIKES]->(l)
    RETURN l`)
}




module.exports = {
    createEquipment,
    deleteEquipment,
    deleteFriend,
    makeFriend,
    dislikeEquipment,
    likesEquipment,
    getAllEquipmentLikedFromFriend
};





