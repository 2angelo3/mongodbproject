const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache')
var User = mongoose.model('User');

const EquipmentSchema = require('./equipment.model');
// we use the review schema in the wishlistschema
// define the wishlist schema
const WishlistSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A wishlist needs a name.']
    },
  
    description: {
        type: String,
        required: true
    }, 
    
    equipment: {
        type: Schema.Types.ObjectId,
        ref: 'Equipment',
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }

   


});





// export the Equipment model through a caching function
module.exports = getModel('Wishlist', WishlistSchema)