const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const Equipment = require('./equipment.model')() // note we need to call the model caching function


describe('Equipment model', function() {
    describe('unit tests', function() {
        it('should reject empty name', async function() {
            const testEquipment = {
                name: "",
                emailAddress: 'Dumbel2@server.nl',
                description: 'De dumbel is 25 kg',
                comments: null,
                user: null
            }
    
            // use validate and not save to make it a real unit test (we don't require a db this way)
            await expect(new Equipment(testEquipment).validate()).to.be.rejectedWith(Error)
        })
    
      
    })
})