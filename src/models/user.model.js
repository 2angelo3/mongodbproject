const mongoose = require('mongoose')
const Schema = mongoose.Schema
var crypto = require('crypto');
const getModel = require('./model_cache')
var jwt = require('jsonwebtoken');


const EquipmentSchema = require('./equipment.model');


const UserSchema = new Schema({
    // a user needs to have a name
    username: {
        type: String,
        required: [true, 'A user needs to have a name.'],
    },
    password: {
        type: String,
        required: [true, 'A user needs to have a password.']
        
    },
    
    email: {
        type: String,
        required: [true, 'A user needs to have a email.']
    },
    description: {
        type: String
    
    
    },

    
    
 

})

// mongoose plugin to always populate fields
UserSchema.plugin(require('mongoose-autopopulate'));

// when a user is deleted all their reviews need to be deleted
// note: use an anonymous function and not a fat arrow function here!
// otherwise 'this' does not refer to the correct object
// use 'next' to indicate that mongoose can go to the next middleware
UserSchema.pre('remove', function(next) {
    // include the match model here to avoid cyclic inclusion
    const Equipment = mongoose.model('Equipment')

    // don't iterate here! we want to use mongo operators!
    // this makes sure the code executes inside mongo
    Match.updateMany({}, {$pull: {'comments': {'user': this._id}}})
        .then(() => next())
})

// export the user model through a caching function
module.exports = getModel('User', UserSchema);


