const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache')

// we use the Commentschema in the Comment schema

// define the Comment schema
// define a schema for comments
const CommentSchema = new Schema({
    title: {
        type: String,
        required: [true, 'A comment needs a title.']
    },
    text: String,


    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },

});







// export the Equipment model through a caching function
// export the comment schema

// module.exports = getModel('Comment', CommentSchema)
module.exports = CommentSchema;