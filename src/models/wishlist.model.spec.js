const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const User = require('./user.model')()
const Wishlist = require('./wishlist.model')()
const Equipment = require('./equipment.model')() // note we need to call the model caching function


describe('Wishlist model', function() {
    describe('unit tests', function() {
        let testUser
        let testEquipment

        beforeEach(async function() {
            testUser = await new User({
                username: 'Angelo',
                email: 'Avp@server.nl',
                password: 'wachtwoord123'
            }).save()
    
            testEquipment = await new Equipment({
                name: 'dumbell 2',
                emailaddress: 'dumbel@server.nl',
                description: 'de dumbel van 25 kg ',
                comments:[],
            }).save()
        })

        it('should reject empty name', async function() {
            const testWishlist = {
                
                description: 'wishlist for the sportequipments',
                equipment: testEquipment,
                user: testUser._id
               
            }
    
            // use validate and not save to make it a real unit test (we don't require a db this way)
            await expect(new Wishlist(testWishlist).validate()).to.be.rejectedWith(Error)
        })
    
      
    })
})