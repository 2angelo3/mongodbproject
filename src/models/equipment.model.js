const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache')

// we use the review schema in the Equipment schema
// define the Equipment schema
const CommentSchema = require('./comment.schema');

const EquipmentSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A Equipment needs a name.']
    },
    emailAddress: {
        type: String,
    },
    description: {
        type: String,
       
    },  
    comments: {
        type: [CommentSchema],
        default: []
    },

    postedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
     
    
});





// export the Equipment model through a caching function
module.exports = getModel('Equipment', EquipmentSchema)