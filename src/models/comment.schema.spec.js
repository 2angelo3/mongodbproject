const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('./user.model')() // note we need to call the model caching function
const Equipment = require('./equipment.model')() // note we need to call the model caching function


describe('Comment schema', function() {
    let user

    beforeEach(async function() {
        user = {
            username: 'Angelo van peer',
            password:'wachtwoord123',
            email:'avp@server.nl'
        }

        const savedUser = await new User(user).save()
        user['id'] = savedUser._id
    })

    describe('unit tests', function() {
        it('Should reject comments without user', async function() {
            const testEquipment = {
                 title: 'SportEquipment',
                 text: 'de 25 kg dumbell',
                 user: 'Angelo van Peer',
               
            }
    
            await expect(new Equipment(testEquipment).save()).to.be.rejectedWith(Error)
        })
    
     
    })
})