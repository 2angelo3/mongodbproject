const chai = require('chai')
const expect = chai.expect
var jwt = require('jsonwebtoken');

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('./user.model')() // note we need to call the model caching function

describe('user model', function() {
    describe('unit tests', function() {
        it('should reject a missing user name', async function() {
            const user = new User({})
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })
    

    
        it('should not create duplicate user names', async function() {
            await new User({username: 'Joe',
            email: "avp@server.nl",
            password: "wachtwoord123",
            description: "22 jaar noord brabant"}).save()
            const user = new User({username: 'Joe',
            email: "avp@server.nl",
            password: "wachtwoord123",
            description: "22 jaar noord brabant"})
            
            await expect(user.save()).to.be.rejectedWith(Error)
    
            let count = await User.find().countDocuments()
            expect(count).to.equal(1)
        })
    })


})

