// avoid duplicate code for connecting to mongoose
const mongoose = require('mongoose')
const neo4j = require('neo4j-driver');

// these options are to not let mongoose use deprecated features of the mongo driver
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
}

async function mongoConnect(dbName) {
    try {
        await mongoose.connect(`${process.env.MONGO_URL}/${dbName}`, options)
        console.log(`connection to DB ${dbName} established`)
    } catch (err) {
        console.error(err)
    }
}

async function neoConnect() {
    this.driver = await neo4j.driver(
        process.env.NEO4J_URL,
        neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
    );

    this.driver.verifyConnectivity().then((res) => {
        console.log(res)
        console.log('connection to Neo4j established')
    }).catch((err) => {
        console.error(err)
        console.error('Neo4j connection failed')
    });
};

function session() {
    return this.driver.session({
        defaultAccessMode: neo4j.session.WRITE
    });
}

module.exports = {mongoConnect, neoConnect, session}